module type HT = Hashtbl.HashedType

module type Token = sig
  include HT

  val fresh_token : unit -> t
end

module Keyserver (Email: HT) (Token: Token) (Key: HT) = struct
  type email = Email.t
  type token = Token.t
  type key   = Key.t

  module Htoken = Hashtbl.Make (Token)

  type state = {
    mutable st_database : (email * key) list;
    st_wait_confirm_add : (email * key) Htoken.t;
    st_wait_confirm_del : (email * key) Htoken.t;
  }

  let initialize_state () = {
    st_database         = [];
    st_wait_confirm_add = Htoken.create 128;
    st_wait_confirm_del = Htoken.create 128;
  }

  let get_exn state email =
    List.assoc email state.st_database

  let get_opt state email =
    List.assoc_opt email state.st_database

  let find_by_exn state email key =
    let eq_key_email (e, k) = Key.equal k key && Email.equal e email in
    List.find eq_key_email state.st_database

  let find_by_opt state email key =
    let eq_key_email (e, k) = Key.equal k key && Email.equal e email in
    List.find_opt eq_key_email state.st_database

  let add state email key =
    let token = Token.fresh_token () in
    Htoken.add state.st_wait_confirm_add token (email, key);
    token

  let confirm_add state token =
    let email, key = Htoken.find state.st_wait_confirm_add token in
    state.st_database <- (email, key) :: state.st_database

  let del_exn state email key =
    let email, key = find_by_exn state email key in
    let token = Token.fresh_token () in
    Htoken.add state.st_wait_confirm_del token (email, key);
    token

  let del_opt state email key =
    match find_by_opt state email key with
    | None -> None
    | Some (email, key) -> let token = Token.fresh_token () in
      Htoken.add state.st_wait_confirm_del token (email, key);
      Some token

  let confirm_del state token =
    let email, key = Htoken.find state.st_wait_confirm_del token in
    let neq_key_email (e, k) = not (Key.equal k key && Email.equal e email) in
    state.st_database <- List.filter neq_key_email state.st_database

end
