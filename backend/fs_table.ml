
open Format
open Unix
open Base64__Base64
open Array

let load (name:string) : unit =
  try
    let st = stat name in
    match st.st_kind with
    | S_DIR -> ()
    | _ ->
       eprintf "Cannot use `%s` as table: it is not a directory@." name;
       exit 2
  with
    e ->
    eprintf "Unexpected exception while loading table `%s`: %s@." name
      (Printexc.to_string e);
    exit 2

let encode k = String.map (fun c -> if c = '/' then '-' else c) (encode k)

let decode k = String.map (fun c -> if c = '-' then '/' else c) (decode k)

let add k v t : unit =
  try
    let k = encode k in
    let fd = openfile (Filename.concat t k) [O_WRONLY;O_CREAT] 0o600 in
    let _n = write_substring fd v 0 (String.length v) in
    close fd
  with
    e ->
    eprintf "Unexpected exception while add in table `%s`, key `%s`: %s@." t k
      (Printexc.to_string e);
    exit 2

let remove k t : unit =
  try
    let k = encode k in
    unlink (Filename.concat t k)
  with
    e ->
    eprintf "Unexpected exception while remove in table `%s`, key `%s`: %s@." t k
      (Printexc.to_string e);
    exit 2

let buf = Bytes.create 1024

let find k t : string =
  try
    let k = encode k in
    let fd = openfile (Filename.concat t k) [O_RDONLY] 0o600 in
    let n = read fd buf 0 1024 in
    close fd;
    Bytes.sub_string buf 0 n
  with
  | Unix_error(ENOENT, "open", _) -> raise Not_found
  | e ->
     eprintf "Unexpected exception while reading table `%s`, key `%s`: %s@." t k
       (Printexc.to_string e);
     exit 2

let mem k t : bool =
  try
    let k = encode k in
    let fd = openfile (Filename.concat t k) [O_RDONLY] 0o600 in
    close fd;
    true
  with
  | Unix_error(ENOENT, "open", _) -> false
  | e ->
    eprintf "Unexpected exception while checking mem in table `%s`, key `%s`: %s@." t k
      (Printexc.to_string e);
    exit 2

let to_array _ : (string * string) array =
  Array.make 0 ("","") (* todo *)
