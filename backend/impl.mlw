module Impl
  use map.Map
  use set.Set as S
  use set.Fset
  use fmap.Fmap
  use table.Table
  use spec.Types
  use int.Int
  use ocaml.Exceptions

  type state = {
    mutable ghost         key: fmap email key;
    mutable ghost confirm_add: fmap token (email, key);
    mutable ghost confirm_del: fmap token (email, key);
                database     : table;
                database_cadd: table;
                database_cdel: table;
               email_to_token: table;
  }
  (* all emails, keys, and tokens in the state are well-formed *)
  invariant { forall e. mem e key ->
              is_email_well_formed e /\ is_key_well_formed key[e] }
  invariant { forall t e k. mapsto t (e, k) confirm_add ->
              is_token_well_formed t /\
              is_email_well_formed e /\ is_key_well_formed k }
  invariant { forall t e k. mapsto t (e, k) confirm_del ->
              is_token_well_formed t /\
              is_email_well_formed e /\ is_key_well_formed k }
  (* email_to_token is inverse of cadd/cdel *)
  invariant { forall t. mem t confirm_add ->
              let (e, _) = confirm_add[t] in
              mapsto (email_to_string e) (token_to_string t) email_to_token }
  invariant { forall t. mem t confirm_del ->
              let (e, _) = confirm_del[t] in
              mapsto (email_to_string e) (token_to_string t) email_to_token }
  invariant { forall e t. mapsto e (token_to_string t) email_to_token ->
              (mem t confirm_add /\ fst confirm_add[t] = e)
              \/ (mem t confirm_del /\ fst confirm_del[t] = e) }
  (* email is part of key *)
  invariant { forall e. mem e key -> is_email_part_of_key e key[e] }
  invariant { forall t. mem t confirm_add -> let (e,k) = confirm_add[t] in
              is_email_part_of_key e k }
  invariant { forall t. mem t confirm_del -> let (e,k) = confirm_del[t] in
              is_email_part_of_key e k }
  (* a same email can't be pending and available at the same time *)
  invariant { forall t. mem t confirm_add -> let (e,_) = confirm_add t in
              not mem e key } (* enough to prove confirm_add_key *)
  (* TBD : the reverse implication holds too *)

  (* no two pending adds for the same email *)
  invariant { forall t1 t2. mem t1 confirm_add -> mem t2 confirm_add ->
              t1 <> t2 -> fst confirm_add[t1] <> fst confirm_add[t2] }
  (* no two pending dels for the same email *)
  invariant { forall t1 t2. mem t1 confirm_del -> mem t2 confirm_del ->
              t1 <> t2 -> fst confirm_del[t1] <> fst confirm_del[t2] }
  (* pending dels are in key *)
  invariant { forall t e k. mapsto t (e, k) confirm_del -> mapsto e k key }
  (* gluing invariants *)
  invariant {
     S.(==) key.domain
            (S.map string_to_email database.domain) /\
     forall e. S.mem e key.domain -> string_to_key (database e) = key e
  }
  invariant {
     S.(==) confirm_add.domain
            (S.map string_to_token database_cadd.domain) /\
     forall t. S.mem t confirm_add.domain ->
       string_to_pair (database_cadd t) = confirm_add t
  }
  invariant {
     S.(==) confirm_del.domain
            (S.map string_to_token database_cdel.domain) /\
     forall t. S.mem t confirm_del.domain ->
       string_to_pair (database_cdel t) = confirm_del t
  }
  by { key = empty; confirm_add = empty; confirm_del = empty;
       database = Table.create (); database_cadd = Table.create ();
       database_cdel = Table.create ();
       email_to_token = Table.create () }

  exception InitializationError
  (* Harder to prove, kept for later *)
  let initialize () : state =
  raises { InitializationError }
    let t1 = Table.load "db_email_key" in
    let (t1_array, t1_ind) = Table.to_array t1 in
    let t4 = Table.load "db_email_to_token" in
    let (t4_array, t4_ind) = Table.to_array t4 in
    for i = 0 to t1_array.Array.length - 1 do
        invariant { forall es. mem es t1.to_fmap -> t1_ind es < i ->
                    let (e,k) = (string_to_email es, string_to_key (t1.to_fmap es)) in
                    is_email_well_formed e
                    /\ is_key_well_formed k
                    /\ is_email_part_of_key e k}
        let (a,b) = t1_array.Array.([]) i in
        let (e,k) = (string_to_email a, string_to_key b) in
        if (not is_email_well_formed e)
           || (not is_key_well_formed k)
           || (not is_email_part_of_key e k)
        then raise InitializationError
    done;
    let t2 = Table.load "db_confirm_add" in
    let (t2_array, t2_ind) = Table.to_array t2 in
    for i = 0 to t2_array.Array.length - 1 do
        invariant { forall ts. mem ts t2.to_fmap -> t2_ind ts < i ->
                    let (t, (e,k)) = (string_to_token ts, string_to_pair (t2.to_fmap ts)) in
                    (is_token_well_formed t /\ is_key_well_formed k /\ is_email_well_formed e /\ is_email_part_of_key e k)
                    /\
                    (mapsto (email_to_string e) ts t4)
                    /\
                    (not mem (email_to_string e) t1) }
        invariant { forall ts1 ts2. mem ts1 t2.to_fmap -> mem ts2 t2.to_fmap ->
                    t2_ind ts1 < i -> t2_ind ts2 < i ->
                    let (e1,_) = string_to_pair (t2.to_fmap ts1) in
                    let (e2,_) = string_to_pair (t2.to_fmap ts2) in
                    ts1 <> ts2 -> e1 <> e2 }
        let (ts,ps) = t2_array.Array.([]) i in
        let (t, (e,k)) = (string_to_token ts, string_to_pair ps) in
        let (es, ks) = (email_to_string e, key_to_string k) in
        if (not is_token_well_formed t)
           || (not is_key_well_formed k)
           || (not is_email_well_formed e)
           || (not is_email_part_of_key e k)
        then raise InitializationError;
       if (not (mem es t4))
           || (not (eq_string (Table.find es t4) ts))
         then raise InitializationError;
       if mem es t1 then
         raise InitializationError;
       for j = 0 to i-1 do
         invariant { forall ts1 ts2. mem ts1 t2.to_fmap -> mem ts2 t2.to_fmap ->
                     j <= t2_ind ts1 < i -> t2_ind ts2 < j ->
                     let (e1,_) = string_to_pair (t2.to_fmap ts1) in
                     let (e2,_) = string_to_pair (t2.to_fmap ts2) in
                     ts1 <> ts2 -> e1 <> e2 }
         let (ts2,ps2) = t2_array.Array.([]) j in
         let es2 = email_to_string (fst (string_to_pair ps2)) in
         if (not eq_string ts ts2) && eq_string es2 es then raise InitializationError;
       done;
    done;
    let t3 = Table.load "db_confirm_del" in
    let (t3_array, t3_ind) = Table.to_array t3 in
    for i = 0 to t3_array.Array.length - 1 do
        invariant { forall ts. mem ts t3.to_fmap -> t3_ind ts < i ->
                    let (t, (e,k)) = (string_to_token ts, string_to_pair (t3.to_fmap ts)) in
                    (is_token_well_formed t /\ is_key_well_formed k /\ is_email_well_formed e /\ is_email_part_of_key e k)
                    /\
                    (mapsto (email_to_string e) ts t4) 
                    /\
                    (mapsto (email_to_string e) (key_to_string k) t1) }
        invariant { forall ts1 ts2. mem ts1 t3.to_fmap -> mem ts2 t3.to_fmap ->
                    t3_ind ts1 < i -> t3_ind ts2 < i ->
                    let (e1,_) = string_to_pair (t3.to_fmap ts1) in
                    let (e2,_) = string_to_pair (t3.to_fmap ts2) in
                    ts1 <> ts2 -> e1 <> e2 }
        let (ts, ps) = t3_array.Array.([]) i in
        let (t, (e,k)) = (string_to_token ts, string_to_pair ps) in
        let (es, ks) = (email_to_string e, key_to_string k) in
        if (not is_token_well_formed t)
           || (not is_key_well_formed k)
           || (not is_email_well_formed e)
           || (not is_email_part_of_key e k)
        then raise InitializationError;
        if (not (mem es t4))
           || (not (eq_string (Table.find es t4) ts))
        then raise InitializationError;
        if not (mem es t1 && (eq_string (find es t1) ks))
        then raise InitializationError;
        for j = 0 to i-1 do
         invariant { forall ts1 ts2. mem ts1 t3.to_fmap -> mem ts2 t3.to_fmap ->
                     j <= t3_ind ts1 < i -> t3_ind ts2 < j ->
                     let (e1,_) = string_to_pair (t3.to_fmap ts1) in
                     let (e2,_) = string_to_pair (t3.to_fmap ts2) in
                     ts1 <> ts2 -> e1 <> e2 }
         let (ts2,ps2) = t3_array.Array.([]) j in
         let es2 = email_to_string (fst (string_to_pair ps2)) in
         if (not eq_string ts ts2) && eq_string es2 es then raise InitializationError;
       done;
    done;
    (*   (* pending dels are in key *)
    invariant { forall t e k. mapsto t (e, k) confirm_del -> mapsto e k key } *)
    for i = 0 to t4_array.Array.length - 1 do
        invariant { forall es ts. mapsto es ts t4.to_fmap -> t4_ind es < i ->
                    (mem ts t2 /\ let (e,_) = string_to_pair (t2.to_fmap ts) in
                                  es = email_to_string e) \/
                    (mem ts t3 /\ let (e,_) = string_to_pair (t3.to_fmap ts) in
                                  es = email_to_string e) }
        let (es, ts) = t4_array.Array.([]) i in
        if not (mem ts t2) && not (mem ts t3) then raise InitializationError;
        if mem ts t2 && mem ts t3 then raise InitializationError;
        if mem ts t2 then
          begin
          let rs = Table.find ts t2 in
          let es2 = email_to_string (fst (string_to_pair rs)) in
          if not eq_string es2 es then raise InitializationError;
          end;
        if mem ts t3 then
          let rs = Table.find ts t3 in
          let es3 = email_to_string (fst (string_to_pair rs)) in
          if not eq_string es3 es then raise InitializationError;
    done;
    let ghost t1_domain = t1.to_fmap.domain in
    let ghost t2_domain = t2.to_fmap.domain in
    let ghost t3_domain = t3.to_fmap.domain in
    let ghost t1_contents = t1.to_fmap.contents in
    let ghost t2_contents = t2.to_fmap.contents in
    let ghost t3_contents = t3.to_fmap.contents in
    let ghost mapt1 a = string_to_key (t1_contents (email_to_string a)) in
    let ghost mapt2 a = string_to_pair (t2_contents (token_to_string a)) in
    let ghost mapt3 a = string_to_pair (t3_contents (token_to_string a)) in
    let ghost k = Fmap.mk (Fset.map string_to_email t1_domain) mapt1 in
    let ghost ca = Fmap.mk (Fset.map string_to_token t2_domain) mapt2 in
    let ghost cd = Fmap.mk (Fset.map string_to_token t3_domain) mapt3 in
    let r = { database = t1 ; database_cadd = t2 ; database_cdel = t3;
      key = k; confirm_add = ca ; confirm_del = cd;
      email_to_token = t4 } in
    r

  val gen_new_token (s: state) : token
    ensures { is_token_well_formed result }
    ensures { not (mem result s.confirm_add) }
    ensures { not (mem result s.confirm_del) }

  let add_key (s: state) (e: email) (k: key) : token
    requires { not (mem e s.key) }
    requires { is_email_well_formed e }
    requires { is_key_well_formed k }
    requires { is_email_part_of_key e k }
    writes   { s.confirm_add, s.database_cadd, s.email_to_token }
    ensures  { not (mem result (old s).confirm_add) }
    ensures  { not (mem result (old s).confirm_del) }
    ensures  { s.confirm_add[result] = (e, k) }
    ensures  { forall t. mem t (old s.confirm_add) ->
               if fst (old s.confirm_add)[t] = e
               then not (mem t s.confirm_add)
               else s.confirm_add[t] = (old s.confirm_add)[t] }
  =
    let tok = gen_new_token s in
    let toks = token_to_string tok in
    let es = email_to_string e in
    if Table.mem es s.email_to_token then begin
      let ts = Table.find es s.email_to_token in
      Table.remove ts s.database_cadd;
      let ghost t = string_to_token ts in
      ghost (s.confirm_add <- Fmap.remove t s.confirm_add);
    end;
    Table.add toks (pair_to_string (e, k)) s.database_cadd;
    ghost (s.confirm_add <- Fmap.add tok (e, k) s.confirm_add);
    Table.add es toks s.email_to_token;
    tok

  let confirm_add_key (s : state) (conf : token) : unit
    requires { mem conf s.confirm_add }
(*    writes   { s.confirm_add, s.key, s.database_cadd, s.database, s.email_to_token }*)
    ensures  { s.confirm_add = remove conf (old s.confirm_add) }
    ensures  { let (e,k) = (old s).confirm_add conf in
               s.key = (old s.key)[e <- k] }
  =
    let p = Table.find (token_to_string conf) s.database_cadd in
    let (e,k) = string_to_pair p in
    Table.remove (token_to_string conf) s.database_cadd;
    Table.remove (email_to_string e) s.email_to_token;
    Table.add (email_to_string e) (key_to_string k) s.database;
    ghost (s.confirm_add <- Fmap.remove conf s.confirm_add);
    ghost (s.key <- Fmap.add e k s.key);
    ()

let delete_key (s: state) (e: email) (k: key) : token
    requires { mapsto e k s.key } (* (e,k) exists in database *)
    writes   { s.confirm_del, s.database_cdel, s.email_to_token }
    ensures  { not (mem result (old s).confirm_add) } (* token is fresh *)
    ensures  { not (mem result (old s).confirm_del) }
    ensures  { s.confirm_del[result] = (e,k) }
    ensures  { forall t. mem t (old s.confirm_del) ->
               if fst (old s.confirm_del)[t] = e
               then not (mem t s.confirm_del)
               else s.confirm_del[t] = (old s.confirm_del)[t] }
  =
    let tok = gen_new_token s in
    let toks = token_to_string tok in
    let es = email_to_string e in
    if Table.mem es s.email_to_token then begin
      let ts = Table.find es s.email_to_token in
      Table.remove ts s.database_cdel;
      let ghost t = string_to_token ts in
      ghost (s.confirm_del <- Fmap.remove t s.confirm_del);
    end;
    Table.add toks (pair_to_string (e, k)) s.database_cdel;
    ghost (s.confirm_del <- Fmap.add tok (e, k) s.confirm_del);
    Table.add es toks s.email_to_token;
    tok

  let confirm_del_key (s: state) (conf: token) : unit
    requires { mem conf s.confirm_del }
    writes   { s.confirm_del, s.key, s.database_cdel, s.email_to_token, s.database }
    ensures  { s.confirm_del = remove conf (old s.confirm_del) }
    ensures  { let (e, _) = (old s).confirm_del conf in
               s.key = remove e (old s.key) }
  =
    let p = Table.find (token_to_string conf) s.database_cdel in
    let (e,_) = string_to_pair p in
    Table.remove (token_to_string conf) s.database_cdel;
    Table.remove (email_to_string e) s.email_to_token;
    Table.remove (email_to_string e) s.database;
    ghost (s.confirm_del <- Fmap.remove conf s.confirm_del);
    ghost (s.key <- Fmap.remove e s.key);
    ()

  let find_key (s: state) (e: email) : key
    requires { mem e s.key }
    ensures { result = s.key[e] }
  = string_to_key (Table.find (email_to_string e) s.database)
end

module ImplCorrect
  use Impl
  clone spec.Spec with type state = state, val find_key = find_key, val add_key = add_key,
                       val confirm_add_key = confirm_add_key, val delete_key = delete_key,
                       val confirm_del_key = confirm_del_key
end
