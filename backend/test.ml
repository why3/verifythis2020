
open Frontend__Frontend
open Format


let interp_line (s: string) : unit =
  let args = String.split_on_char ' ' s in
  match args with
  | ["G" ; email ] ->
     begin try
         let k = get email in
         printf "Get email:`%s` -> key:`%s`@." email k
       with Not_found ->
         printf "Get email:`%s` -> not found@." email
     end
  | ["A" ; email ; key ] ->
     let tok = add email key in
     printf "Add email:`%s` key:`%s` -> token:`%s`@." email key tok
  | ["C" ; tok ] ->
     begin try
         let () = confirm_add tok in
         printf "ConfirmAdd token:`%s` -> ()@." tok
       with Not_found ->
         printf "ConfirmAdd token:`%s` -> not found@." tok
     end
  | ["D" ; email ; key ] ->
     let tok = delete email key in
     printf "Delete email:`%s` key:`%s` -> token:`%s`@." email key tok
  | ["K" ; tok ] ->
     begin try
         let () = confirm_del tok in
         printf "ConfirmDel token:`%s` -> ()@." tok
       with Not_found ->
         printf "ConfirmDel token:`%s` -> not found@." tok
     end
  | _ ->
     printf "@[<h 2>Bad request, must be one of the forms:@\n\
          for get: G email@\n\
          for add: A email key@\n\
          for confirm add: C token@\n\
          for del: D email key@\n\
          for confirm del: K token@]@."

let () =
  try
    while true do
      let s = read_line () in
      interp_line s
    done
  with End_of_file -> ()
