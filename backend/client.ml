open Verifythis2020_middle

module Token = struct
  type t = int
  let equal = (=)
  let hash x = Hashtbl.hash x
  let fresh_token =
    let c = ref 0 in
    fun () -> incr c; !c
end

module HashString = struct
  include String
  let hash s = Hashtbl.hash s
end

open Backend

module KS = Keyserver(HashString)(Token)(HashString)

let () =
  let state = KS.initialize_state () in
  let email = "mjp.pereira@fct.unl.pt" in
  let key   = "12345" in
  let token = KS.add state email key in
  (* sending and reception of the mail *)
  KS.confirm_add state token;
  begin match KS.get_exn state email with
  | key' -> assert (String.equal key key')
  | exception Not_found -> assert false end;
  begin match KS.del_exn state email key with
  | token -> KS.confirm_del state token
  | exception Not_found -> assert false end;
  begin match KS.get_exn state email with
  | _key' -> assert false
  | exception Not_found -> Format.printf "SUCCESS!@." end
