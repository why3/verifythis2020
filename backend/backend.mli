module type HT = Hashtbl.HashedType

module type Token = sig
  include HT

  val fresh_token : unit -> t
end

module Keyserver (Email: HT) (Token: Token) (Key: HT) : sig
  type email = Email.t
  type token = Token.t
  type key   = Key.t

  type state

  val initialize_state : unit -> state

  val get_exn : state -> email -> key
  val get_opt : state -> email -> key option

  val del_exn     : state -> email -> key -> token
  val del_opt     : state -> email -> key -> token option
  val confirm_del : state -> token -> unit

  val add         : state -> email -> key -> token
  val confirm_add : state -> token -> unit
end
