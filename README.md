# Why3 VerifyThis 2020 Participation #

An implementation of a key server using Why3 and OCaml

Supports:

- HTTP server implementing the 5 operations
- email/key pairs stored permanently in the file system

Does not support:

- cryptographic security

# How to compile #

Typing `make` alone will recompile some files using dune (to be
documented)

Type `make extract` to extract the Why3 code of `backend/impl.mlw` and
`backend/frontend.mlw` to OCaml

# How to run

the first time you need to type `makes databases` to initialize the
databases. Afterwards the code can be run by typing `./runtest`

# How to replay the proofs #

type `make replay`. It will replay the proofs made in files
`backend/impl.mlw` and `backend/frontend.mlw`

# Database #

 On filesystem:
  - `keys/XX/$email/$sha1` file contains the `key`
  - `confirmed/XX/$nonce` file contains `date of garbage collecting`
