\documentclass[article]{llncs} %% FIXME: enlever draft

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{hyperref}

\begin{document}
\sloppy
\hbadness=9999

% much better for submission, to be removed in final version
\pagestyle{plain}

\title{``You-Know-Why'': an Early-Stage Prototype of a Key Server Developed using Why3}

\author{Diego Diverio\inst{1} \and Cláudio Lourenço\inst{2} \and Claude Marché\inst{2} }

\institute{
  % official signature for Inria team common with LRI
  % https://www.universite-paris-saclay.fr/fr/signature
  % Université Paris-Saclay, Univ. Paris-Sud, CNRS, Inria, Laboratoire de recherche en informatique, 91405, Orsay, France
  Université Paris-Saclay, Inria, 91120 Palaiseau, France
  \and
  Université Paris-Saclay, Univ. Paris-Sud, CNRS, Inria, LRI, 91405 Orsay, France
}

\maketitle

\begin{abstract}
This is a preliminary report on a solution we designed for the
VerifyThis Collaborative Long Term Challenge 2019~\cite{vtltc}.
\end{abstract}

\section{Used verification approach and tools}

% Why3, ocaml code

We used the Why3 verification framework~\cite{bobot14sttt} to design,
from scratch, a simple but running prototype implementation of a PGP
key server. The current version does not have a Web interface but a basic
command-line interface. We exploit the ability of Why3 to extract
OCaml code from verified WhyML code (getting rid of formal
specifications and \emph{ghost} code~\cite{gondelman16fmsd}) so as to
produce code that can be compiled into an executable.

Our prototype is made of a combination of unverified handwritten
OCaml code and of WhyML code whose functional behaviour is formally
specified and proven correct. As Why3 is a framework for sequential
programs only, our approach does not address any issue related to
concurrency of execution of server requests.

\section{How the challenge was adapted to make verification possible}

% (abstractions, reimplementation, different behaviour)

The main approach we took was to follow the document presenting the
challenge~\cite{huisman19vtltc}. We also had a look at the
implementation of the Hagrid key server~\cite{hagrid} written in Rust, to get
more precise ideas on its implementation. In particular, we discovered
that it was using the file system of the host computer as a database,
instead of any more sophisticated database management system.

From the challenge document~\cite{huisman19vtltc} we addressed mission
1 (Safety), mission 2 (Functionality) and mission 6 (Termination). The
mission 0 (Identify Relevant Properties) was not deeply investigated:
we turned the informal properties of the five operations given in the
challenge documentation~\cite[Section 5]{huisman19vtltc} into formal
specifications in WhyML. For that we reused the theories for sets and
maps from the Why3 standard library. Yet it appeared that such theories
were in fact under reorganisation and we are using the new versions,
so that replaying our proofs today requires to install the Why3
version in the master branch of development\footnote{Or presumably the
  upcoming next release 1.3.0.}. We also used character strings, which
is a newly available theory in the Why3 standard library.

We did not really address mission 3 (Protocol), though our functional
specifications implicitly express properties about the server
protocol, such as the fact that to confirm a key addition, only a
token previously issued by the \textsc{Add} operation can be accepted. Missions
4 (Privacy), 5 (Thread Safety) and 7 (Randomness) are not addressed at all.

We also did not implement a Web front-end though we could have done it
with unverified OCaml code.

\section{What has been achieved}

% (modelled and verified properties)

Our implementation is divided into three parts as it is described in
the challenge presentation~\cite{huisman19vtltc}: a front-end (here a
basic CLI program), a back-end (which actually implements the
requirements) and a database (here a file-based implementation of
dictionaries). The development is publicly available from the
\textsc{gitlab} repository
\url{https://gitlab.inria.fr/why3/verifythis2020}. The \texttt{README}
file there explains how to replay the proofs, how to compile an
executable and how to run the latter.

The back-end interface is specified by a WhyML module \texttt{Spec}
containing an abstract view of the server global \emph{state} - under
the form of a record type with ghost fields and invariants - and the
expected five operations \textsc{Query}, \textsc{Add},
\textsc{ConfirmAdd}, \textsc{Del} and \textsc{ConfirmDel}. The
back-end is implemented in another module \texttt{Impl} which refines
\texttt{Spec}, the type \emph{state} being extended with concrete
fields. Gluing invariants relate those concrete fields with the ghost
fields. The refinement is proved correct. The proofs are not
difficult: they proceed smoothly using automated provers only.

The module \texttt{Impl} makes use of an extra module \texttt{Table}
providing an interface to a general data structure of dictionaries mapping strings
to strings. The \texttt{Table} module is only a WhyML interface, its
implementation is written in OCaml and is not verified. Yet, this
OCaml code makes use of an implementation of \texttt{Base64} encoding-decoding
that is used to turn arbitrary strings into valid filenames. This
\texttt{Base64} module is a completely independent library that was recently
verified with Why3. This constitutes the database part of our prototype.

While the back-end operations rely on preconditions to ensure
properties on inputs, the front-end adds a layer which checks inputs
and raises exceptions when they do not conform to preconditions. The
front-end is written in WhyML but is actually completed with OCaml code
that provides the simple CLI interface. The front-end also implements
an initialisation function that performs sanity checks on database
when starting the server (see details below).

% In order to extract the WhyML code, we had to write a specific
% `driver`, a file describing some transformations from WhyML code to
% OCaml code, to help the extractor.

% We abstracted the requirements in an interface (file spec.mlw) bearing
% all the logic invariants, and implemented it in a derived file
% (impl.mlw) which contains the actual program that respects the
% invariants. The clone directive of WhyML is used in order to ensure
% that the implementation conforms to the specification.

%% Might be interesting to write the paper in a ~historic
%% perspective :p

% We successfully modelled the intended behaviour for the functions
% given in the source document. We left unproven, as for now, the
% initialization of the data structures from the disk.

% It would be great to develop more proven code, like a proven code for
% a HTTP frontend, and to prove properties about the cryptographic
% primitives.

\section{Successes and challenges}

% encountered in the course of the case study.

The main success is that we could actually write the specifications
down in WhyML, develop WhyML code that is fully proven conforming to
the specifications, and also extract an executable program from it
that we can interface with OCaml external code, to effectively run a
basic prototype server.

The proofs were not particularly challenging. Each verification
condition was most of the time discharged by one of the automated
theorem provers available in Why3 (one of Alt-Ergo, CVC4, or Z3 for
this proof). Adding a few extra assertions in the code was enough in
the remaining cases but one. This extra case has to be handled using a
few manual Why3 transformations to be discharged~\cite{dailler2018},
but was quite easy to prove anyway. The most challenging part was in
fact the initialisation function: the code of this function must
indeed do a lot of checks on the data read from disc, so as to ensure
that the resulting server state satisfies the invariant. It is also
the only part of the code using loops, thus requiring loop invariants
to be added.

Of course, the remaining challenges are numerous, starting from the
fact that we did not address at all the issues related to concurrency,
privacy, or randomness. Another challenge would be to get closer to the
current Hagrid server which uses clever techniques to store less
information on disc. For example, it encodes all the needed information
in the confirmation tokens: Hagrid does not have to store any
information about the token, as it can recover it from the token itself,
whereas we have to store locally for each token which email and key it
was associated with.

Another challenge in a more general point of view is the amount of
time, or human effort, to achieve such a verified server: we had quite
little human resources to dedicate to this case study, and we had to
aim at a very simple implementation if we wanted to have a working
basic prototype. Designing a verified alternative to Hagrid, with all
required guarantees regarding privacy and concurrency, should start
by planning significant allocation of human resources.

\subsection*{Acknowledgements}

For their contributions and feedback, we would like to thank François
Bobot, Sylvain Dailler, Jean-Christophe Filliâtre and Mário
Pereira.

\bibliographystyle{splncs04}
\bibliography{abbrevs,demons,demons2,demons3,team,crossrefs,local}

\end{document}

\appendix

\section{Step-by-step development}

\subsection{Writing the specification}

We started by translating the requirements of the VerifyThis
specification in an equivalent specification that we could write in
our target language: WhyML. For each function of the source
specification, we created and adapted the requirements. As an example,
we provide here such a translation for the function confirming an
addition to the database:

\begin{lstlisting}
  val confirm_add_key (s: state) (conf: token) : unit
    requires { mem conf s.confirm_add }
    writes   { s.confirm_add, s.key}
    ensures  { s.confirm_add = remove conf (old s.confirm_add) }
    ensures  { let (e,k) = (old s).confirm_add conf in
               s.key = (old s.key)[e <- k] }
\end{lstlisting}

In addition to adapting the requirements, we had to devise a way to
capture the state of the program. Such a state would respect
invariants that make it consistant in regard to the specification of
the functions.

A simple invariant here:

\begin{lstlisting}
  (* no two pending dels for the same email *)
  invariant { forall t1 t2. mem t1 confirm_del ->
              mem t2 confirm_del ->
              t1 <> t2 ->
              fst confirm_del[t1] <> fst confirm_del[t2] }
\end{lstlisting}

\subsection{Writing a proven implementation of the specification}

After creating the requirements for the diverse functions, we wrote
an implementation in WhyML, respecting the logical requirements of the
written specification. We has to use ghost values to enforce some
behaviours, and added some invariants to the state, so to say ``gluing
invariants'' to make sure the ghost values and the program values are
always in sync.

An example of such an implementation:
\begin{lstlisting}
let find_key (s: state) (e: email) : key
    requires { not (mem e s.key) }
    ensures { result = s.key[e] }
= string_to_key (Table.find (email_to_string e) s.database)
\end{lstlisting}

In order to prove that the implementation indeed captures the
specification, we used the clone directive of WhyML.

\begin{lstlisting}
module ImplCorrect
  use Impl
  clone spec.Spec with type state = state, val find_key = find_key,
  val add_key = add_key, val confirm_add_key = confirm_add_key,
  val delete_key = delete_key, val confirm_del_key = confirm_del_key
end
\end{lstlisting}

\subsection{Proving the code}

Now that we have an implementation, we can try to actually prove
it. Using the Why3 environment, we were able to prove all goals we
wanted to prove.

%% More details coming

\subsection{Extracting the code}

The goal being not staying in the realms of logic but actually having
executable code at the end of the process, we had to extract the WhyML
code to OCaml. To do so, we had to write a piece of code called a
``driver'' which helps translate the WhyML code (that has already a
ML-like syntax) to OCaml. An excerpt of the driver is shown here:

\begin{lstlisting}
module table.Table
  (* the type table is extracted to "string *)
  syntax val mem "(Fs_table.mem %1 %2)"
  syntax val add "(Fs_table.add %1 %2 %3)"
\end{lstlisting}

The extracted code is pure OCaml code, which can be called from any
OCaml program, leading us to the next section.

\subsection{Writing executable code calling the proven code}

We wrote a little command-line based program as a test frontend for
our implementation.

\section{Architecture}

In order to model the program, we used 4 association tables
program-side, and 3 association tables logic-side. We defined several
invariants on them to keep the synchronicity between the program-side
values and the logical ones.

The list of the logical invariants we wanted to enforce all over the
execution of the program:
\begin{itemize}
\item All emails, keys and tokens must be well-formed.
\item The pending confirmation tables are inverse tables of the table
  linking emails to tokens.
\item Emails must be inferred from keys (as it is in PGP).
\item A same email can't be pending and available at the same time.
\item A same email can't be pending twice, be it for insertion or
  removal or both.
\item The pending deletions must be in the database.
\end{itemize}

% Giving out all the invariants we used?
% The specifications of the functions?-


\end{document}


% Local Variables:
% compile-command: "make -C ."
% mode: latex
% TeX-master: "main"
% TeX-PDF-mode: t
% mode: flyspell
% ispell-local-dictionary: "british"
% End:

%  LocalWords:  OCaml Hagrid VerifyThis invariants provers WhyML
