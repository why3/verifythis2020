(* server_example.ml *)
open Lwt
(* open Cohttp *)
open Cohttp_lwt_unix

let server =
  let callback _conn req body =
    (* let uri = req |> Request.uri |> Uri.to_string in *)
    let query = req |> Request.uri |> Uri.query in
    (* let meth = req |> Request.meth |> Code.string_of_method in *)
    (* let headers = req |> Request.headers |> Header.to_string in *)
    body |> Cohttp_lwt.Body.to_string >|= (fun _body ->
        Format.asprintf "@[<v>%a@]"
          (Fmt.list (Fmt.box (Fmt.pair ~sep:Fmt.sp Fmt.string (Fmt.list ~sep:Fmt.semi Fmt.string))))
          query
      )
    >>= (fun body -> Server.respond_string ~status:`OK ~body ())
  in
  Server.create ~mode:(`TCP (`Port 8000)) (Server.make ~callback ())

let () = ignore (Lwt_main.run server)
