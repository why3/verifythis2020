.PHONY: all clean

all:
	dune build @all verifythis2020.opam

replay:
	why3 -L backend replay backend/base64
	why3 -L backend replay backend/impl
	why3 -L backend replay backend/frontend

extract: extracted/frontend__Frontend.ml extracted/base64__Base64.ml

extracted/base64__Base64.ml:  backend/base64.mlw
	mkdir -p extracted
	why3 extract --modular -D ocaml64 -o extracted backend/base64.mlw

extracted/frontend__Frontend.ml: backend/table.mlw backend/impl.mlw backend/extra.drv
	mkdir -p extracted
	why3 extract --modular --recursive \
		-L backend \
		-D ocaml64 \
		-D backend/extra.drv \
		-o extracted \
		backend/frontend.mlw

FILES=extracted/base64__Base64.ml \
	backend/fs_table.ml \
	extracted/table__Table.ml \
	extracted/spec__Types.ml \
	extracted/impl__Impl.ml \
	extracted/frontend__Frontend.ml \
	backend/test.ml

runtest: $(FILES)
	ocamlopt -I backend -I extracted -I `ocamlfind query zarith` zarith.cmxa -o runtest unix.cmxa $(FILES)

databases:
	mkdir -p db_email_key db_confirm_add db_confirm_del db_email_to_token

clean:
	dune clean
